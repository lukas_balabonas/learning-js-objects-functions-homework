const SearchModule = (() => {

    let input, 
        output,
        value;

    const _updateOutput = () => {
        value = input.value;
        console.log(value);
        output.innerHTML = value;
    }

    const initialize = (querySelectorInput, querySelectorOutput) => {
        input = document.querySelector(querySelectorInput);
        output = document.querySelector(querySelectorOutput);
        console.log('running init', input, output);
        input.addEventListener('keyup', _updateOutput);
    };

    return {
        initialize: initialize
    };

})();

SearchModule.initialize('.input', '.message');
